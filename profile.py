"""
Profile for sharing blockstore datasets via http.
"""

import geni.portal as portal
import geni.rspec.pg as rspec


portal.context.defineParameter("dataset",
                               "URN of dataset to share",
                               portal.ParameterType.STRING, "urn:publicid:IDN+emulab.net:project+ltdataset+dataset")


params = portal.context.bindParameters()
request = portal.context.makeRequestRSpec()

node = request.DockerContainer("node")
node.docker_extimage = "nginx-autoindex:1.21.6"
node.docker_extserver = "docker-registry.k8s.flux.utah.edu"
node.exclusive = False
node.routable_control_ip = True

if params.dataset != "urn:publicid:IDN+emulab.net:project+ltdataset+dataset":
    # Blockstore to share
    iface = node.addInterface()
    fsnode = request.RemoteBlockstore("fsnode", "/usr/share/nginx/html")
    fsnode.dataset = params.dataset
    fsnode.readonly = True

    fslink = request.Link("fslink")
    fslink.addInterface(iface)
    fslink.addInterface(fsnode.interface)

    fslink.best_effort = True
    fslink.vlan_tagging = True


portal.context.printRequestRSpec()
